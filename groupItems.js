// Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        //a b c d k

const data = require('./itemData');

function groupItem(accumulator,currentValue){
    
    if(currentValue.contains.includes("Vitamin A")){
        if(!accumulator["Vitamin A"]){
            accumulator["Vitamin A"]  = [];
            
        }
        accumulator["Vitamin A"].push(currentValue.name);
    }
    if(currentValue.contains.includes("Vitamin B")){
        if(!accumulator["Vitamin B"]){
            accumulator["Vitamin B"]  = [];
        }
        accumulator["Vitamin B"].push(currentValue.name);
    }
    if(currentValue.contains.includes("Vitamin C")){
        if(!accumulator["Vitamin C"]){
            accumulator["Vitamin C"]  = []
        }
        accumulator["Vitamin C"].push(currentValue.name);
    }
    if(currentValue.contains.includes("Vitamin D")){
        if(!accumulator["Vitamin D"]){
            accumulator["Vitamin D"]  = []
        }
        accumulator["Vitamin D"].push(currentValue.name);
    }
    else{
        if(!accumulator["Vitamin K"]){
            accumulator["Vitamin K"]  = []
        }
        accumulator["Vitamin K"].push(currentValue.name);
    }
    return accumulator;
} 


const group = data.reduce(groupItem,{});
console.log(group);