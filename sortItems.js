const data = require('./itemData');

// Sort items based on number of Vitamins they contain.
function calculateTotalVitamins(item){
    if (item.contains === false) {
      return 0;
    }
    return item.contains.split(',').map(vitamin => vitamin.trim()).length;
  };
  
  // Sorting items based on the total number of vitamins in descending order
  const sortedItems = data.sort((a, b) => {
    const vitaminsA = calculateTotalVitamins(a);
    const vitaminsB = calculateTotalVitamins(b);
    return vitaminsB - vitaminsA;
  });
  
  //it prints output
  console.log(sortedItems);